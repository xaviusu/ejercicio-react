import { useState }  from 'react';
import Resultado from './Resultado';
function Principal(props) {
    const [entrada, setEntrada] = useState(0);
    const [resultado, setResultado] = useState(0);

    const multiplicar37 = function() {
        let result = entrada * 37
        document.getElementById("btn37").style.backgroundColor = "#000000";
        document.getElementById("btn37").style.color = "#FFFFFF";
        document.getElementById("btn43").style.backgroundColor = "#FFFFFF";
        document.getElementById("btn43").style.color = "#000000";
        setResultado(result);
        console.log(result);
    }
    const multiplicar43 = function() {
        let result = entrada * 43
        document.getElementById("btn43").style.backgroundColor = "#000000";
        document.getElementById("btn43").style.color = "#FFFFFF";
        document.getElementById("btn37").style.backgroundColor = "#FFFFFF";
        document.getElementById("btn37").style.color = "#000000";
        setResultado(result);
        console.log(result);
    }

    return (
        <div className="card" style={{width: "35rem", margin: "auto", marginTop: "5rem"}}>
            <div className="card-body" style={{textAlign: "center"}} >
                <h5 className="card-title" >Calculadora</h5>
                <input type="number" value={entrada} onChange={(e) => {setEntrada(e.target.value)}}></input>
                <br/>
                <button onClick={multiplicar37} id="btn37">x37</button>
                <button onClick={multiplicar43} id="btn43">x43</button>
            </div>
            <div className="card-footer">
                <Resultado resultado={resultado}/>
            </div>
        </div>
    )
}

export default Principal;