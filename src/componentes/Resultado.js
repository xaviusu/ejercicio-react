import { useState }  from 'react';

function Resultado(props) {
    return (
        <div style={{textAlign: "center"}}>
            {props.resultado}
        </div>
    )
}

export default Resultado;